extends Node

const USER_SETTINGS_PATH = "user://user.settings"

const SETTINGS_USERNAME = "username"
const SETTINGS_PASSWORD = "password"

var data

func _ready():
	load_data()
		
func get_user_settings_file() -> File:
	var file = File.new()
	
	if not file.file_exists(USER_SETTINGS_PATH):
		file.open(USER_SETTINGS_PATH, File.WRITE)
		file.store_line("{}")
		file.close()
	
	file.open(USER_SETTINGS_PATH, File.READ_WRITE)
		
	return file
	
func load_data():
	
	var file = get_user_settings_file()
		
	var json = JSON.parse(file.get_as_text())
	
	if json.error :
		print("Unable to load user settings")
		file.store_line("{}")
		json.result = {}
	
	self.data = json.result
	
	file.close()
	
func get_data(key):
	return data[key] if data.has(key) else null
	
func set_data(key, value):
	data[key] = value
	
func save_data():
	var file = get_user_settings_file()
	file.store_line(JSON.print(data))
	file.close()
