#ifndef IA_H
#define IA_H

#include <Godot.hpp>
#include <Node.hpp>
#include <vector>
#include <unordered_map>

using std::unordered_map;
using std::vector;

namespace godot
{

struct ia_piece
{
    int type;
    int side;
    int original_type;
};

struct ia_square
{
    int pos_x;
    int pos_y;
    int directions;
    ia_piece *piece;
};

struct ia_game
{
    bool over;
    int turn;
    int width;
    int height;
    vector<vector<ia_square *>> board;
    long long int hash;
    long long int hash2;
};

struct ia_move
{
    ia_piece *piece_from;
    ia_square *square_from;
    ia_piece *piece_to;
    ia_square *square_to;
    float score;
};

enum ia_entry_type
{
    exact,
    lower_bound,
    upper_bound
};

struct ia_hash
{
    int depth;
    ia_entry_type entry_type;
    float eval;
    long long int hash;
};

class Move : public Reference
{
    GODOT_CLASS(Move, Reference)

public:
    static void _register_methods();
    Ref<Reference> piece_from;
    Ref<Reference> square_from;
    Ref<Reference> piece_to;
    Ref<Reference> square_to;
    float score;
    Move();
    ~Move();
    void _init();
};

class IA : public Node
{
    GODOT_CLASS(IA, Node)

private:
    const int dirs[8]{128, 1, 2, 64, 4, 32, 16, 8};
    float activity_weight = 0.3f;
    float material_weight = 0.7f;
    vector<vector<vector<long long int>>> *hashrandom;
    vector<vector<vector<long long int>>> *hashrandom2;
    float tolerance = -1;
    vector<ia_move *> history;
    vector<String> board_history_hashes;
    unordered_map<long long int, ia_hash> hashes;
    vector<vector<int>> square_values;
    bool should_move;
    int algorithm;
    int evaluation;
    float dummy_value;
    float nully_value;

public:
    static void _register_methods();

    IA();
    ~IA();
    void _init();

    unsigned long long int compute_hash(ia_game &board);
    void think(Ref<Reference> game);
    void _exit_tree();
    void _ready();
    void set_activity_weight(float activity_weight);
    float get_activity_weight();
    void set_material_weight(float material_weight);
    float get_material_weight();
    ia_square *convert_godot_square_to_ia_square(Ref<Reference> square);
    ia_piece *convert_godot_piece_to_ia_piece(Ref<Reference> piece);
    vector<vector<ia_square *>> convert_godot_board_to_ia_board(Ref<Reference> board);
    vector<ia_move> get_moves(ia_game &game, int turn);
    String ia_move_to_str(ia_move *m);
    String hash_board(vector<vector<ia_square *>> board);
    void print_board(ia_game &game);
    void xor_piece_in_square(ia_game &game, ia_piece *piece, ia_square *square);
    bool check_nullity(vector<vector<ia_square *>> m);
    float pvs_with_transposition(ia_game &game, int turn, float alpha, float beta, int current_depth);
    float alpha_beta(ia_game &game, int turn, float minimum, int current_depth);
    float pvs(ia_game &game, int turn, float alpha, float beta, int current_depth);
    void play_move(ia_move &m, ia_game &game);
    void reset_move(ia_move &m, ia_game &game);
    float evaluate(ia_game &game, int depth);
    float evaluate_with_square_values(ia_game &game, int depth);
    void register_move(Ref<Reference> _piece, Ref<Reference> square_from, Ref<Reference> square_to, Ref<Reference> board_model);
    void clean_history();
    void print_move(Ref<Move> m);
    void sort(vector<ia_move> &moves, int i = -1);
    vector<ia_square *> get_available_squares_for_piece(ia_piece *piece, ia_square *square, ia_game &game, int for_turn);
    int count_available_squares_for_piece(ia_piece *piece, ia_square *square, ia_game &game, int for_turn = -1);
    bool is_available(int x, int y, ia_piece *piece, ia_game &game);
    bool add_if_available(int x, int y, ia_piece *piece, ia_game &game, vector<ia_square *> &available_squares);
    void _on_TimerMove_timout();
};

} // namespace godot

#endif
