#include "ia.h"
#include <cmath>
#include <OS.hpp>
#include <math.h>
#include <RandomNumberGenerator.hpp>
#include <Timer.hpp>

using namespace godot;

void Move::_register_methods()
{
    register_property<Move, Ref<Reference>>("piece_from", &Move::piece_from, NULL);
    register_property<Move, Ref<Reference>>("square_from", &Move::square_from, NULL);
    register_property<Move, Ref<Reference>>("piece_to", &Move::piece_to, NULL);
    register_property<Move, Ref<Reference>>("square_to", &Move::square_to, NULL);
    register_property<Move, float>("score", &Move::score, 0);
}

Move::Move()
{
}

Move::~Move()
{
}

void Move::_init()
{
}

void IA::_register_methods()
{
    register_property<IA, float>("tolerance", &IA::tolerance, 0, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_DEFAULT, GODOT_PROPERTY_HINT_RANGE, "-1, 5");
    register_signal<IA>("moves_found", "move", GODOT_VARIANT_TYPE_ARRAY);
    register_property<IA, float>("activity_weight", &IA::set_activity_weight, &IA::get_activity_weight, 0.3);
    register_property<IA, float>("material_weight", &IA::set_activity_weight, &IA::get_activity_weight, 0.7);
    register_property<IA, int>("algorithm", &IA::algorithm, 0, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_DEFAULT, GODOT_PROPERTY_HINT_ENUM, "PVS, PVS with transposition, alphabeta");
    register_property<IA, int>("evaluation", &IA::evaluation, 0, GODOT_METHOD_RPC_MODE_DISABLED, GODOT_PROPERTY_USAGE_DEFAULT, GODOT_PROPERTY_HINT_ENUM, "Activity,Square values");
    register_property<IA, float>("dummy_value", &IA::dummy_value, 1.0);
    register_property<IA, float>("nully_value", &IA::nully_value, 7.0);

    register_method("think", &IA::think);
    register_method("_ready", &IA::_ready);
    register_method("register_move", &IA::register_move);
    register_method("_on_TimerMove_timeout", &IA::_on_TimerMove_timout);
}

IA::IA()
{
}

IA::~IA()
{
}

void IA::_init()
{
    activity_weight = 0.3;
    material_weight = 0.7;
    tolerance = -1;
    vector<ia_move *> history;
    vector<String> board_history_hashes;
    RandomNumberGenerator *rng = RandomNumberGenerator::_new();
    hashrandom = new vector<vector<vector<long long int>>>;
    for (int i = 0; i < 7; i++)
    {
        vector<vector<long long int>> hashrandomi;
        for (int j = 0; j < 8; j++)
        {
            vector<long long int> hashrandomj;
            for (int k = 0; k < 6; k++)
            {
                hashrandomj.push_back(rng->randi());
            }
            hashrandomi.push_back(hashrandomj);
        }
        hashrandom->push_back(hashrandomi);
    }
    hashrandom2 = new vector<vector<vector<long long int>>>;
    for (int i = 0; i < 7; i++)
    {
        vector<vector<long long int>> hashrandomi;
        for (int j = 0; j < 8; j++)
        {
            vector<long long int> hashrandomj;
            for (int k = 0; k < 6; k++)
            {
                hashrandomj.push_back(rng->randi());
            }
            hashrandomi.push_back(hashrandomj);
        }
        hashrandom2->push_back(hashrandomi);
    }
}

void IA::_ready()
{
    get_node("TimerMove")->connect("timeout", this, "_on_TimerMove_timeout");
}

unsigned long long int IA::compute_hash(ia_game &game)
{
    unsigned long long int h = 0;
    for (int i = 0; i < game.width; i++)
    {
        for (int j = 0; j < game.height; j++)
        {
            ia_piece *piece = game.board[i][j]->piece;
            if (piece != NULL)
            {
                int piece_index = piece->type + piece->side * 3;

                h ^= (*hashrandom)[i][j][piece_index];
            }
        }
    }
    return h;
}

void IA::register_move(Ref<Reference> _piece, Ref<Reference> square_from, Ref<Reference> square_to, Ref<Reference> board_model)
{
    ia_move *move = new ia_move;
    move->square_to = convert_godot_square_to_ia_square(square_to);
    move->square_from = convert_godot_square_to_ia_square(square_from);
    history.push_back(move);
    vector<vector<ia_square *>> board = convert_godot_board_to_ia_board(board_model);
    board_history_hashes.push_back(hash_board(board));
}

ia_square *IA::convert_godot_square_to_ia_square(Ref<Reference> s)
{
    ia_square *square = new ia_square;
    square->pos_x = (int)s->get("pos_x");
    square->pos_y = (int)s->get("pos_y");
    square->piece = convert_godot_piece_to_ia_piece(s->call("get_piece"));
    square->directions = (int)s->get("directions");
    return square;
}

ia_piece *IA::convert_godot_piece_to_ia_piece(Ref<Reference> p)
{
    if (p != NULL)
    {
        ia_piece *piece = new ia_piece;
        piece->original_type = (int)p->get("original_type");
        piece->side = (int)p->get("side");
        piece->type = (int)p->get("type");
        return piece;
    }

    return NULL;
}

void IA::set_activity_weight(float _activity_weight)
{
    activity_weight = _activity_weight;
    material_weight = 1 - _activity_weight;
}

float IA::get_activity_weight()
{
    return activity_weight;
}

void IA::set_material_weight(float _material_weight)
{
    material_weight = _material_weight;
    activity_weight = 1 - material_weight;
}

float IA::get_material_weight()
{
    return material_weight;
}

void IA::_exit_tree()
{
    delete hashrandom;
    delete hashrandom2;
}

void IA::clean_history()
{
    for (int i = 0; i < history.size(); i++)
    {
        ia_move *move = history[i];
        delete move->piece_from;
        delete move->square_from;
        delete move->piece_to;
        delete move->square_to;
        delete move;
    }
    history.resize(0);
    board_history_hashes.resize(0);
}

bool IA::check_nullity(vector<vector<ia_square *>> board)
{
    String hash = hash_board(board);
    int found_count = 1;
    for (int i = 0; i < board_history_hashes.size(); i++)
    {
        if (hash == board_history_hashes[i])
        {
            found_count++;
        }
    }
    return found_count >= 3;
}

vector<vector<ia_square *>> IA::convert_godot_board_to_ia_board(Ref<Reference> board_model)
{
    int width = (int)board_model->get("width");
    int height = (int)board_model->get("height");
    vector<vector<ia_square *>> board;
    // Build custom version of the board and everything for performance
    for (int i = 0; i < width; i++)
    {
        vector<ia_square *> row;
        for (int j = 0; j < height; j++)
        {
            ia_square *square = new ia_square;
            square->pos_x = i;
            square->pos_y = j;
            Ref<Reference> s = board_model->call("get_square", i, j);
            Ref<Reference> p = s->call("get_piece");
            if (p != NULL)
            {
                ia_piece *piece = new ia_piece;
                piece->side = (int)p->get("side");
                piece->type = (int)p->get("type");
                piece->original_type = piece->type;
                square->piece = piece;
            }
            else
            {
                square->piece = NULL;
            }
            square->directions = s->get("directions");
            row.push_back(square);
        }
        board.push_back(row);
    }
    return board;
}

void IA::think(Ref<Reference> g)
{
    Godot::print("searching... ");
    should_move = false;
    (Object::cast_to<Timer>(get_node("TimerMove")))->start();
    int turn = g->get("turn");
    Ref<Reference> board_model = g->get("board_model");

    vector<vector<ia_square *>> board = convert_godot_board_to_ia_board(board_model);

    ia_game game;
    game.over = false;
    game.board = board;
    game.turn = turn;
    game.width = (int)board_model->get("width");
    game.height = (int)board_model->get("height");
    long long int hashsum = compute_hash(game);
    game.hash = hashsum;
    for (int i = 0; i < game.width; i++)
    {
        vector<int> values_row;
        for (int j = 0; j < game.height; j++)
        {
            int value = 0;
            for (int k = 0; k < 8; k++)
            {
                if (board[i][j]->directions & dirs[k])
                {
                    value++;
                }
            }
            values_row.push_back(value);
        }
        square_values.push_back(values_row);
    }

    int time = OS::get_singleton()->get_system_time_msecs();

    vector<ia_move> moves = get_moves(game, turn);
    // for (int i = 0; i < moves.size(); i++)
    // {
    //     ia_move &m = moves[i];
    //     play_move(m, game);
    //     if (check_nullity(game.board))
    //     {
    //         Godot::print("nullity from alpha");
    //         m.score = -1000;
    //     }
    //     else
    //     {
    //         m.score = alpha_beta(game, (turn + 1) % 2, -100000, depth - 1);
    //         //m.score = -pvs(game, (turn + 1) % 2, -100000, 100000, first_depth - 1) ;
    //     }
    //     reset_move(m, game);
    // }
    int d;
    for (d = 1; d < 15; d++)
    {

        float alpha = -100000;
        float beta = 100000;
        int i;
        for (i = 0; i < moves.size(); i++)
        {

            ia_move &m = moves[i];
            play_move(m, game);

            if (check_nullity(game.board))
            {

                Godot::print("nullity from pvs");
                m.score = -1000;
            }
            else
            {
                if (algorithm == 0)
                {

                    if (i == 0)
                    {
                        m.score = -pvs(game, (turn + 1) % 2, -beta, -alpha, d - 1);
                    }
                    else
                    {
                        m.score = -pvs(game, (turn + 1) % 2, -alpha - 1, -alpha, d - 1);
                        if (alpha < m.score && m.score < beta)
                        {
                            m.score = -pvs(game, (turn + 1) % 2, -beta, -m.score, d - 1);
                        }
                    }
                }
                else if (algorithm == 1)
                {
                    if (i == 0)
                    {
                        m.score = -pvs_with_transposition(game, (turn + 1) % 2, -beta, -alpha, d - 1);
                    }
                    else
                    {
                        m.score = -pvs_with_transposition(game, (turn + 1) % 2, -alpha - 1, -alpha, d - 1);
                        if (alpha < m.score && m.score < beta)
                        {
                            m.score = -pvs_with_transposition(game, (turn + 1) % 2, -beta, -m.score, d - 1);
                        }
                    }
                }
                else
                {
                    m.score = alpha_beta(game, (turn + 1) % 2, -beta, d - 1);
                }
            }

            reset_move(m, game);
            // if (algorithm == 0)
            // {
            //     alpha = fmaxf(alpha, m.score);

            //     if (alpha >= beta)
            //         break;
            // }
            // Godot::print("{0}", i);
            if (should_move)
            {
                i++;
                break;
            }
        }
        sort(moves, i);

        if (should_move)
        {
            break;
        }

        int time2 = OS::get_singleton()->get_system_time_msecs();
        time = time2;
    }

    Godot::print("depth {0}, evaluation {1}", d - 1, evaluation);

    float best_score = moves[0].score;

    Array godot_moves = Array();

    for (int i = 0; i < moves.size(); i++)
    {
        ia_move m = moves[i];
        Move *move = Move::_new();
        move->square_from = board_model->call("get_square", m.square_from->pos_x, m.square_from->pos_y);
        move->square_to = board_model->call("get_square", m.square_to->pos_x, m.square_to->pos_y);
        move->piece_from = move->square_from->call("get_piece");
        move->piece_to = move->square_to->call("get_piece");
        move->score = m.score;
        godot_moves.push_back(move);
        print_move(move);
        // if(tolerance < 0 && i == 0 || best_score - m.score >= tolerance) {
        //     break;
        // }
    }
    // godot_moves.shuffle();

    // for (int i = 0; i < godot_moves.size(); i++)
    // {
    //     print_move(godot_moves[i]);
    // }

    for (int i = 0; i < game.width; i++)
    {
        for (int j = 0; j < game.height; j++)
        {
            if (board[i][j]->piece != NULL)
            {
                delete board[i][j]->piece;
                delete board[i][j];
            }
        }
    }

    Godot::print(String("new moves {0}!!").format(Array::make(moves.size())));
    emit_signal("moves_found", Array::make(godot_moves));

    for (int i = 0; i < godot_moves.size(); i++)
    {
        Move *move = godot_moves[i];
        delete move;
    }
}

void IA::sort(vector<ia_move> &moves, int s)
{
    if (s == -1)
    {
        s = moves.size();
    }
    bool do_sort = false;
    do
    {
        do_sort = false;
        for (int i = 0; i < s - 2; i++)
        {
            ia_move m1 = moves[i];
            ia_move m2 = moves[i + 1];
            if (m1.score < m2.score)
            {
                ia_move tmp = m1;
                moves[i] = m2;
                moves[i + 1] = tmp;
                do_sort = true;
            }
        }
    } while (do_sort);
}

void IA::print_board(ia_game &game)
{
    String out = "";
    for (int i = 0; i < game.height; i++)
    {
        for (int j = 0; j < game.width; j++)
        {
            ia_piece *piece = game.board[j][i]->piece;
            if (piece != NULL)
            {
                out += String("{0}").format(Array::make(piece->type + piece->side * 3));
            }
            else
            {
                out += ".";
            }
        }
        out += "\n";
    }
    Godot::print(out);
}

vector<ia_move> IA::get_moves(ia_game &game, int turn)
{
    vector<vector<ia_square *>> board = game.board;
    vector<ia_move> moves;
    int width = game.width;
    int height = game.height;
    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            ia_square *square = board[x][y];
            ia_piece *piece = square->piece;
            if (piece == NULL)
            {
                continue;
            }
            if (piece->side == turn)
            {
                vector<ia_square *> available_squares = get_available_squares_for_piece(piece, square, game, turn);
                for (int i = 0; i < available_squares.size(); i++)
                {
                    ia_square *s = available_squares[i];
                    ia_move m;
                    m.piece_from = piece;
                    m.square_from = square;
                    m.piece_to = s->piece;
                    m.square_to = s;
                    moves.push_back(m);
                }
            }
        }
    }
    return moves;
}

vector<ia_square *> IA::get_available_squares_for_piece(ia_piece *piece, ia_square *square, ia_game &game, int for_turn)
{
    int turn = for_turn != -1 ? for_turn : game.turn;

    if (piece->side != turn || game.over)
    {
        return vector<ia_square *>(0);
    }

    int piece_type = piece->type;

    int posX = square->pos_x;
    int posY = square->pos_y;

    vector<ia_square *> available_squares;

    int mask = 0;

    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (i == 0 && j == 0)
            {
                continue;
            }
            int dir = dirs[mask];
            mask++;
            if (!(dir & square->directions))
            {
                continue;
            }
            int x = posX + j;
            int y = posY + i;
            if (piece_type == 1)
            {
                while (true)
                {
                    if (!(add_if_available(x, y, piece, game, available_squares)))
                    {
                        break;
                    }
                    x += j;
                    y += i;
                }
            }
            else
            {
                add_if_available(x, y, piece, game, available_squares);
            }
        }
    }
    return available_squares;
}

int IA::count_available_squares_for_piece(ia_piece *piece, ia_square *square, ia_game &game, int for_turn)
{
    int turn = for_turn;

    int piece_type = piece->type;

    int posX = square->pos_x;
    int posY = square->pos_y;

    int mask = 0;

    int count = 0;

    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            if (i == 0 && j == 0)
            {
                continue;
            }
            int dir = dirs[mask];
            mask++;
            if (!(dir & square->directions))
            {
                continue;
            }
            int x = posX + j;
            int y = posY + i;
            if (piece_type == 1)
            {
                while (true)
                {
                    if (!(is_available(x, y, piece, game)))
                    {
                        break;
                    }

                    count++;

                    x += j;
                    y += i;
                }
            }
            else if (is_available(x, y, piece, game))
            {
                count++;
            }
        }
    }
    return count;
}

bool IA::add_if_available(int x, int y, ia_piece *piece, ia_game &game, vector<ia_square *> &available_squares)
{
    if (x < 0 || x >= game.width || y < 0 || y >= game.height)
    {
        return false;
    }

    ia_square *neighboor_square = game.board[x][y];

    ia_piece *neighboor_piece = neighboor_square->piece;

    if (neighboor_piece != NULL && (neighboor_piece->side == piece->side))
    {
        return false;
    }
    available_squares.push_back(neighboor_square);
    return neighboor_piece == NULL;
}

bool IA::is_available(int x, int y, ia_piece *piece, ia_game &game)
{
    if (x < 0 || x >= game.width || y < 0 || y >= game.height)
    {
        return false;
    }

    ia_square *neighboor_square = game.board[x][y];

    ia_piece *neighboor_piece = neighboor_square->piece;

    if (neighboor_piece != NULL && (neighboor_piece->side == piece->side))
    {
        return false;
    }

    return neighboor_piece == NULL;
}

void IA::print_move(Ref<Move> m)
{
    Godot::print(String("{0} {1} -> {2} {3} : {4}").format(Array::make(m->square_from->get("pos_x"), m->square_from->get("pos_y"), m->square_to->get("pos_x"), m->square_to->get("pos_y"), m->score)));
}

String IA::ia_move_to_str(ia_move *m)
{
    return String("{0} {1} -> {2} {3};").format(Array::make(m->square_from->pos_x, m->square_from->pos_y, m->square_to->pos_x, m->square_to->pos_y));
}

String IA::hash_board(vector<vector<ia_square *>> b)
{
    String hash = "";
    for (int i = 0; i < b.size(); i++)
    {
        for (int j = 0; j < b[i].size(); j++)
        {
            ia_square *s = b[i][j];
            if (s->piece != NULL)
            {
                hash += String("{0}").format(Array::make(s->piece->type + (3 * s->piece->side)));
            }
            else
            {
                hash += ".";
            }
        }
        hash += "\n";
    }
    //return hash;
    return hash.md5_text();
}

float IA::alpha_beta(ia_game &game, int turn, float minimum, int current_depth)
{
    if (game.over || current_depth == 0)
    {
        if (evaluation == 0)
        {
            return evaluate(game, current_depth) * (turn == game.turn ? -1 : 1);
        }
        else
        {
            return evaluate_with_square_values(game, current_depth) * (turn == game.turn ? -1 : 1);
        }
    }

    float j = -100000;
    vector<ia_move> moves = get_moves(game, turn);
    for (int i = 0; i < moves.size(); i++)
    {
        ia_move m = moves[i];

        play_move(m, game);

        j = fmaxf(j, alpha_beta(game, (turn + 1) % 2, j, current_depth - 1));

        reset_move(m, game);

        if (-j <= minimum)
        {
            return -j;
        }
    }
    return -j;
}

float IA::pvs(ia_game &game, int turn, float alpha, float beta, int current_depth)
{
    if (current_depth == 0 || game.over)
    {
        if (evaluation == 0)
        {
            return evaluate(game, current_depth) * (turn == game.turn ? 1 : -1);
        }
        else
        {
            return evaluate_with_square_values(game, current_depth) * (turn == game.turn ? 1 : -1);
        }
    }

    vector<ia_move> moves = get_moves(game, turn);
    float score;
    for (int i = 0; i < moves.size(); i++)
    {
        ia_move m = moves[i];
        play_move(m, game);

        if (i == 0)
        {
            score = -pvs(game, (turn + 1) % 2, -beta, -alpha, current_depth - 1);
        }
        else
        {
            score = -pvs(game, (turn + 1) % 2, -alpha - 1, -alpha, current_depth - 1);
            if (alpha < score && score < beta)
            {
                score = -pvs(game, (turn + 1) % 2, -beta, -score, current_depth - 1);
            }
        }

        reset_move(m, game);

        alpha = fmaxf(alpha, score);

        if (alpha >= beta)
            break;
    }
    return alpha;
}

float IA::pvs_with_transposition(ia_game &game, int turn, float alpha, float beta, int current_depth)
{

    float alpha_orig = alpha;
    if (current_depth >= 4)
    {
        std::unordered_map<long long int, ia_hash>::const_iterator e = hashes.find(game.hash);
        if (e != hashes.end() && e->second.hash == game.hash2 && e->second.depth >= current_depth)
        {
            ia_hash h = e->second;
            float score = h.eval;
            switch (h.entry_type)
            {
            case exact:
                return score;

            case lower_bound:
                alpha = fmaxf(alpha, score);
                break;
            case upper_bound:
                beta = fminf(beta, score);
                break;
            }
            if (alpha >= beta)
            {
                return score;
            }
        }
    }
    if (current_depth == 0 || game.over)
    {
        if (evaluation == 0)
        {
            return evaluate(game, current_depth) * (turn == game.turn ? 1 : -1);
        }
        else
        {
            return evaluate_with_square_values(game, current_depth) * (turn == game.turn ? 1 : -1);
        }
    }

    vector<ia_move> moves = get_moves(game, turn);
    float score = -100000;
    for (int i = 0; i < moves.size(); i++)
    {
        ia_move m = moves[i];
        play_move(m, game);
        if (i == 0)
        {
            score = fmaxf(score, -pvs_with_transposition(game, (turn + 1) % 2, -beta, -alpha, current_depth - 1));
        }
        else
        {
            score = fmaxf(score, -pvs_with_transposition(game, (turn + 1) % 2, -alpha - 1, -alpha, current_depth - 1));
            if (alpha < score && score < beta)
            {
                score = fmaxf(score, -pvs_with_transposition(game, (turn + 1) % 2, -beta, -alpha, current_depth - 1));
            }
        }

        reset_move(m, game);

        alpha = fmaxf(alpha, score);

        if (alpha >= beta)
            break;
    }
    if (current_depth >= 3)
    {
        ia_hash h;

        h.eval = score * (turn == game.turn ? 1 : -1);
        h.depth = current_depth;
        h.hash = game.hash2;

        if (score <= alpha)
        {
            h.entry_type = lower_bound;
        }
        else if (score >= beta)
        {
            h.entry_type = upper_bound;
        }
        else
        {
            h.entry_type = exact;
        }
        hashes[game.hash] = h;
    }
    return score;
}

void IA::play_move(ia_move &m, ia_game &game)
{
    m.square_from->piece = NULL;
    m.square_to->piece = m.piece_from;
    if (m.piece_to != NULL)
    {
        if (m.piece_to->type == 2)
        {
            game.over = true;
        }
        xor_piece_in_square(game, m.piece_to, m.square_to);
    }

    xor_piece_in_square(game, m.piece_from, m.square_from);

    if (m.piece_from->type == 0)
    {
        if (m.piece_from->side == 0 && m.square_to->pos_y == 0 && m.square_from->pos_y != 0)
        {
            m.piece_from->type = 1;
        }
        else if (m.piece_from->side == 1 && m.square_to->pos_y == game.height - 1 && m.square_from->pos_y != game.height - 1)
        {
            m.piece_from->type = 1;
        }
    }
    xor_piece_in_square(game, m.piece_from, m.square_to);
}

void IA::reset_move(ia_move &m, ia_game &game)
{
    m.square_from->piece = m.piece_from;
    m.square_to->piece = m.piece_to;
    xor_piece_in_square(game, m.piece_from, m.square_to);
    if (m.piece_to != NULL)
    {
        if (m.piece_to->type == 2)
        {
            game.over = false;
        }
        xor_piece_in_square(game, m.piece_to, m.square_to);
    }
    if (m.piece_from->original_type == 0)
    {
        if (m.piece_from->side == 0 && m.square_to->pos_y == 0 && m.square_from->pos_y != 0)
        {
            m.piece_from->type = m.piece_from->original_type;
        }
        else if (m.piece_from->side == 1 && m.square_to->pos_y == game.height - 1 && m.square_from->pos_y != game.height - 1)
        {
            m.piece_from->type = m.piece_from->original_type;
        }
    }
    xor_piece_in_square(game, m.piece_from, m.square_from);
}

void IA::xor_piece_in_square(ia_game &game, ia_piece *piece, ia_square *square)
{
    game.hash ^= (*hashrandom)[square->pos_x][square->pos_y][piece->type + piece->side * 3];
    game.hash2 ^= (*hashrandom2)[square->pos_x][square->pos_y][piece->type + piece->side * 3];
}

void IA::_on_TimerMove_timout()
{
    should_move = true;
}

float IA::evaluate(ia_game &game, int depth)
{
    vector<vector<ia_square *>> board = game.board;

    int turn = game.turn;
    float score_activity = 0;
    float score_material = 0;
    int brain_found = 0;

    for (int x = 0; x < game.width; x++)
    {
        for (int y = 0; y < game.height; y++)
        {
            ia_square *square = board[x][y];
            ia_piece *piece = square->piece;

            if (piece == NULL)
            {
                continue;
            }

            int piece_type = piece->type;

            if ((int)piece->side == turn)
            {
                if (piece_type == 2)
                {
                    brain_found += 1;
                }
                else if (piece_type == 0)
                {
                    score_material += dummy_value;
                }
                else if (piece_type == 1)
                {
                    score_material += nully_value;
                }
                score_activity += count_available_squares_for_piece(piece, square, game, game.turn);
            }
            else
            {
                if (piece_type == 2)
                {
                    brain_found -= 1;
                }
                else if (piece_type == 0)
                {
                    score_material -= dummy_value;
                }
                else if (piece_type == 1)
                {
                    score_material -= nully_value;
                }
                score_activity -= count_available_squares_for_piece(piece, square, game, (game.turn + 1) % 2);
            }
        }
    }

    float score = activity_weight * score_activity + material_weight * score_material;

    score += (brain_found * (1 + (0.1 * depth))) * 10000;

    return score;
}

float IA::evaluate_with_square_values(ia_game &game, int depth)
{

    int turn = game.turn;
    float score = 0.0f;
    int brain_found = 0;

    for (int x = 0; x < game.width; x++)
    {
        for (int y = 0; y < game.height; y++)
        {
            ia_square *square = game.board[x][y];
            ia_piece *piece = square->piece;

            if (piece == NULL)
            {
                continue;
            }

            int piece_type = piece->type;

            if (piece->side == turn)
            {
                if (piece_type == 2)
                {
                    brain_found += 1;
                }
                else if (piece_type == 0)
                {
                    score += float(dummy_value) + 0.1f * float(square_values[x][y]);
                }
                else if (piece_type == 1)
                {
                    score += float(nully_value) + 0.3f * float(square_values[x][y]);
                }
            }
            else
            {
                if (piece_type == 2)
                {
                    brain_found -= 1;
                }
                else if (piece_type == 0)
                {
                    score -= float(dummy_value) + 0.1f * float(square_values[x][y]);
                }
                else if (piece_type == 1)
                {
                    score -= float(nully_value) + 0.3f * float(square_values[x][y]);
                }
            }
        }
    }

    score += (float(brain_found) * (1.0f + (0.1f * depth))) * 10000.0f;

    return score;
}