extends Node


# Called when the node enters the scene tree for the first time.
func _ready():
	get_game().referee.connect("game_over", self, "_on_referee_game_over")

func get_game():
	return $BasicGame.get_game()
	
func _on_referee_game_over(winner, _reason, _referee):
	$BasicGame.get_node("TimeUpdate").stop()
	$CanvasLayer3/Panel.show()
	$CanvasLayer3/Panel/LabelGameOver.text = "%s wins in %s moves!" % [winner,get_game().move_count]
	
