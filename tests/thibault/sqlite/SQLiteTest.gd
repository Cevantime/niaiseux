extends Node

var SQLiteScript = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")

# Called when the node enters the scene tree for the first time.
func _ready():
	var SQLite = SQLiteScript.new()
	SQLite.path = "res://tests/thibault/sqlite/db"
	SQLite.open_db()
	
	SQLite.verbose_mode = true
	
	SQLite.drop_table("user")
	
	SQLite.create_table("user", {
		"id": {"data_type":"int", "primary_key": true, "not_null": true},
		"username" : {"data_type":"char(50)"},
		"password" : {"data_type":"char(50)"},
		"salt": {"data_type": "char(50)"},
		"elo": {"data_type":"int", "not_null": true},
	})
	
	SQLite.insert_rows("user", [{
		"username": 'toto',
		"password": 'tata',
		"elo": 1114
	}, {
		"username": "alekhine",
		"password": "alek",
		"elo": 2554
	}])
	
	SQLite.query("SELECT * FROM user")
	
	print(SQLite.query_result)
	
	SQLite.close_db()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
