extends Control

var basic_game_packed_scene = preload("res://tests/thibault/LocalBasicGame.tscn")
var human_player_packed_scene = preload("res://models/player/HumanPlayer.tscn")
var ia_player_packed_scene = preload("res://models/player/IAPlayer.tscn")

func _on_White_pressed():
	new_game_between(human_player_packed_scene.instance(), ia_player_packed_scene.instance(), false)
	
func _on_Black_pressed():
	new_game_between(ia_player_packed_scene.instance(), human_player_packed_scene.instance(), true)

func new_game_between(white_player, black_player, reversed):
	var two_player_scene = basic_game_packed_scene.instance()
	two_player_scene.get_game().set_player_black(black_player)
	two_player_scene.get_game().set_player_white(white_player)
	
	var root = get_node("/root")
	root.get_child(root.get_child_count() - 1).queue_free()
	
	get_node("/root").add_child(two_player_scene)
	
	two_player_scene.get_node("BasicGame").set_reversed(reversed)
	two_player_scene.get_game().start()
