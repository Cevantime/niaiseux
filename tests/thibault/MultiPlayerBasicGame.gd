extends Node

var challenge_element_packed = preload("res://tests/thibault/ChallengeElement.tscn")
var basic_game_packed = preload("res://tests/thibault/BasicGame.tscn")
var player_infos

# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerVariables.load_data()
	$CanvasLayer/WaitingForNetwork.show()
	
	yield($MultiPlayerClient, "connected_to_server")
	
	$MultiPlayerClient.connect("login_successfull", self, "_on_login_successfull")
	
	if not PlayerVariables.get_data(PlayerVariables.SETTINGS_USERNAME):
		$CanvasLayer/CreateOrLogin.show()
	else:
		$MultiPlayerClient.send_msg("login", {
			"username": PlayerVariables.get_data(PlayerVariables.SETTINGS_USERNAME),
			"password": PlayerVariables.get_data(PlayerVariables.SETTINGS_PASSWORD)
		})

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func join_challenges():
	$CanvasLayer/CreateOrJoinMenu.show()
	$CanvasLayer/WaitingForNetwork.hide()
	$CanvasLayer/CreateOrLogin.hide()
	$MultiPlayerClient.connect("challenges_obtained", self, "_on_challenges_obtained")
	$MultiPlayerClient.connect("move_done", self, "_on_move_done")
	$MultiPlayerClient.connect("game_over", self, "_on_game_over")
	$MultiPlayerClient.send_msg("request_challenges", {})

func get_game():
	return get_basic_game().get_node('GameModel')

func _on_MultiPlayerClient_game_joined(game_infos, is_white, opponent):
	get_basic_game().queue_free()
	var new_basic_game = basic_game_packed.instance()
	new_basic_game.name = "BasicGame"
	call_deferred("add_child", new_basic_game, true)
	yield(get_tree(), "idle_frame")
	get_basic_game().connect("human_moved", self, "_on_BasicGame_human_moved")
	$CanvasLayer/CreateOrJoinMenu.hide()
	$CanvasLayer/WaitingForAcceptChallenge.hide();
	$CanvasLayer/CreateOrLogin.hide()
	var board_model = get_basic_game().get_node('CanvasLayer2/Board/BoardModel')
	var new_board = BoardModel.new()
	new_board.create_from_infos(game_infos)
	
	var human_player = HumanPlayer.new()
	var networked_player = NetworkedPlayer.new()
	networked_player.client = $MultiPlayerClient
	get_basic_game().set_reversed(!is_white)
	get_basic_game().set_player1("%s (%s)" % [player_infos.username, player_infos.elo])
	get_basic_game().set_player2("%s (%s)" % [opponent.username, opponent.elo])
	
	var game_model = get_basic_game().get_node("GameModel")
	game_model.set_player_white(human_player if is_white else networked_player)
	game_model.set_player_black(human_player if not is_white else networked_player)
	game_model.milliseconds_for_white = game_infos.milliseconds_for_white
	game_model.milliseconds_for_black = game_infos.milliseconds_for_black
	game_model.inc_milliseconds_for_white = game_infos.inc_milliseconds_for_white
	game_model.inc_milliseconds_for_black = game_infos.inc_milliseconds_for_black
	game_model.start()
				
			
func get_basic_game():
	return get_tree().get_nodes_in_group("BasicGame")[0]

func _on_BasicGame_human_moved(from, to):
	$MultiPlayerClient.send_msg("move_done", {
		"move": {
			"from": from,
			"to": to
		}
	})

func _on_login_successfull(infos):
	player_infos = infos
	join_challenges()

func _on_challenges_obtained(challenges):
	var challenge_list = $CanvasLayer/CreateOrJoinMenu/VBoxContainer/ScrollContainer/VBoxContainer
	for child in challenge_list.get_children():
		child.queue_free()
	
	for challenge in challenges:
		var challenge_element = challenge_element_packed.instance()
		challenge_element.challenge_id = challenge.id
		challenge_element.get_node("Label").text = "%s (%s min + %s s)" % [challenge.client_username, int(challenge.time) / 60000, int(challenge.increment) / 1000]
		challenge_list.add_child(challenge_element)
		challenge_element.connect("challenge_joined", self, "_on_challenge_accepted")
		
func _on_challenge_accepted(challenge_id):
	$MultiPlayerClient.send_msg("accept_challenge", {"challenge_id": challenge_id})

func _on_CreateGameButton_pressed():
	$CanvasLayer/CreateOrJoinMenu.hide()
	$CanvasLayer/SetUpChallenge.show()
	
func _on_move_done(infos):
	yield(get_tree(),"idle_frame")
	get_basic_game().get_node("GameModel").referee.remaining_times = infos.remaining_times
	
func _on_game_over(infos):
	get_basic_game().get_node("TimeUpdate").stop()
	$CanvasLayer3/Panel.show()
	$CanvasLayer3/Panel/LabelGameOver.text = "%s wins in %s moves!" % [infos.winner,infos.move_count]
	player_infos.elo = infos.new_elo
	yield(get_tree().create_timer(2.0), "timeout")
	$CanvasLayer3/Panel.hide()
	$CanvasLayer/CreateOrJoinMenu.show()

func _on_CancelChallengeButton_pressed():
	$MultiPlayerClient.send_msg("cancel_challenge", {})
	$CanvasLayer/CreateOrJoinMenu.show()
	$CanvasLayer/WaitingForAcceptChallenge.hide()


func _on_ValidateButton_pressed():
	var credentials = {
		"username": $CanvasLayer/CreateOrLogin/VBoxContainer/GridContainer/UsernameLineEdit.text,
		"password": $CanvasLayer/CreateOrLogin/VBoxContainer/GridContainer/PasswordLineEdit.text
	}
	
	$MultiPlayerClient.send_msg("create_profile", credentials)
	
	PlayerVariables.set_data(PlayerVariables.SETTINGS_USERNAME, credentials.username)
	PlayerVariables.set_data(PlayerVariables.SETTINGS_PASSWORD, credentials.password)
	PlayerVariables.save_data()
	

func _on_CreateChallengeButton_pressed():
	var time = int($CanvasLayer/SetUpChallenge/VBoxContainer/GridContainer/TimeValue.text) * 60000
	var inc = int($CanvasLayer/SetUpChallenge/VBoxContainer/GridContainer/IncValue.text) * 1000
	$MultiPlayerClient.send_msg("create_challenge", {
		"time" : time,
		"increment" : inc
	})
	$CanvasLayer/SetUpChallenge.hide()
	$CanvasLayer/WaitingForAcceptChallenge.show()
