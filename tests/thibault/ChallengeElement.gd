extends HBoxContainer

signal challenge_joined(challenge_id)

var challenge_id

func _on_JoinButton_pressed():
	emit_signal("challenge_joined", challenge_id)
