tool
extends Node2D

export(float, 5, 100) var square_dim = 60 setget set_square_dim

export(Color) var color1 = Color.white
export(Color) var color2 = Color.black
export(int, 0, 20) var board_width = 0 setget set_board_width
export(int, 0, 20) var board_height = 0 setget set_board_height

onready var board_model: BoardModel = $BoardModel

var square_packed = preload("res://tests/thibault/scenes/Square.tscn")

signal move_done

func _ready():
	board_model.width = board_width
	board_model.height = board_height
	for square in $Squares.get_children():
		var pos: Vector2 = (square.position - Vector2(square_dim, square_dim) / 2) / square_dim
		var i = int(round(pos.x))
		var j = int(round(pos.y))
		board_model.set_square(i, j, square.square_model)
		square.color = color1 if (i % 2 == 0 and j % 2 != 0) or (i % 2 != 0 and j % 2 == 0) else color2
		square.width = square_dim
	
func _process(_delta):
	if(!board_model):
		board_model = $BoardModel
		
func set_squares():
	for square in $Squares.get_children():
		square.queue_free()
	for i in range(board_model.width):
		for j in range(board_model.height):
			create_square(i, j)
			
func adapt_squares(w1, w2, h1, h2):
	if w1 < w2:
		for i in range(w1, w2):
			for j in range(board_model.height):
				create_square(i, j)
	if h1 < h2 : 
		for i in range(0, w2):
			for j in range(h1, h2):
				create_square(i, j)
			
	var squares = $Squares.get_children()
			
	if w1 > w2:
		for square in squares:
			var pos: Vector2 = (square.position - Vector2(square_dim, square_dim) / 2) / square_dim
			if pos.x < w1 and pos.x >= w2:
				square.queue_free()
	if h1 > h2:
		for square in squares:
			var pos: Vector2 = (square.position - Vector2(square_dim, square_dim) / 2) / square_dim
			if pos.y < h1 and pos.y >= h2:
				square.queue_free()
				

func adapt_squares_size(old_square_dim, new_square_dim):
	if not is_instance_valid($Squares):
		return
	var squares = $Squares.get_children()
	for square in squares:
		var pos: Vector2 = (square.position - Vector2(old_square_dim, old_square_dim) / 2) / old_square_dim
		square.width = new_square_dim
		square.position = new_square_dim * Vector2(round(pos.x), round(pos.y)) + Vector2(new_square_dim, new_square_dim) / 2

	
func create_square(i, j):
	var square = square_packed.instance()
	square.width = square_dim
	square.position = square_dim * Vector2(i, j) + Vector2(square_dim, square_dim) / 2
	$Squares.add_child(square)
	square.color = color1 if (i % 2 == 0 and j % 2 != 0) or (i % 2 != 0 and j % 2 == 0) else color2 
	square.set_owner(get_tree().get_edited_scene_root())
	board_model.set_square(i, j, square.square_model)

func set_board_width(bw):
	board_width = bw
	if is_instance_valid(board_model) : 
		var old = board_model.width
		board_model.width = bw
		adapt_squares(old, bw, board_height, board_height)
	
func set_board_height(bh):
	board_height = bh
	if is_instance_valid(board_model) : 
		var old = board_model.height
		board_model.height = bh
		adapt_squares(board_width, board_width, old, bh)

func set_square_dim(_square_dim):
	adapt_squares_size(square_dim, _square_dim)
	square_dim = _square_dim
		
func _on_BoardModel_piece_moved(_piece, square_model_from, square_model_to, board_model):
	var square_to
	var piece_to_move
	for square in $Squares.get_children():
		if square.square_model == square_model_from:
			piece_to_move = square.get_piece()
			square.detach_piece()
		elif square.square_model == square_model_to:
			square.remove_piece()
			square_to = square
	
	square_to.set_piece(piece_to_move)
	
	emit_signal("move_done", square_model_from, square_model_to)
