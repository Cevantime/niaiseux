tool
extends Area2D
class_name Square

enum PIECE_TYPES {
	NONE, DUMMY, NULLY, GENIUS
}

enum PIECE_SIDE {
	LIGHT, DARK
}

enum STATES {
	IDLE,
	SELECTED,
	HIGHLIGHTED
}

export(PIECE_TYPES) var piece setget set_piece_type
export(PIECE_SIDE) var piece_side setget set_piece_side

onready var square_model = $SquareModel

signal selected(square)

var color = Color.white setget set_color
var width = 100 setget set_width
var entered = false
var state setget set_state

export(int, FLAGS, "UP", "LEFT UP", "LEFT", "BOTTOM LEFT", "BOTTOM","RIGHT BOTTOM","RIGHT","UP RIGHT") var directions = 1 setget set_directions

func _ready():
	set_width(width)
	set_directions(directions)
	set_piece_type(piece)
	set_piece_side(piece_side)
	
func _process(_delta):
	if not Engine.editor_hint:
		return
	if !square_model:
		square_model = $SquareModel
	
	
func set_color(_color: Color):
	color = _color
	$Draw.update()
	var intensity = (0.299 * color.r + 0.587 * color.g + 0.114 * color.b)
	var arr_color = Color.white if intensity < 0.5 else Color.black
	for arr in $Arrows.get_children():
		arr.modulate = arr_color
	
func set_width(_width: int):
	width = _width
	var shape = $CollisionShape2D.shape as RectangleShape2D
	shape.extents = Vector2(width, width) / 2
	get_piece().scale = (Vector2(width, width)  / 100) * 0.1
	for arr in $Arrows.get_children():
		arr.scale = (Vector2(width, width)  / 100) * Vector2(0.1, 0.05)
	$Draw.update()
	
func set_directions(_directions):
	directions = _directions
	if has_node("Arrows"):
		for i in range(8):
			var arr = get_node("Arrows/Arrow%s" % [i + 1])
			var p = int(pow(2, i))
			if p & directions:
				arr.show()
			else :
				arr.hide()
			
	if is_instance_valid(square_model):
		square_model.directions = _directions
	
func set_piece_type(_piece):
	if _piece == PIECE_TYPES.NONE:
		get_piece().hide()
		if is_instance_valid(square_model):
			square_model.piece = null
	else:
		get_piece().show()
		get_piece().scale = (Vector2(width, width)  / 100) * 0.1
		get_piece().type = _piece - 1
		get_piece().side = piece_side
		if is_instance_valid(square_model):
			square_model.piece = get_piece().piece_model
	piece = _piece

func get_piece():
	return $PieceSlot.get_child($PieceSlot.get_child_count() - 1) if $PieceSlot.get_child_count() > 0 else null
	
func set_piece(_piece):
	remove_piece()
	$PieceSlot.add_child(_piece)
	_piece.set_owner($PieceSlot)
	if $PieceSlot.get_child_count() > 1:
		$PieceSlot.get_child(0).call_deferred("queue_free")
	# $PieceSlot.call_deferred("remove_child", $PieceSlot.get_child(0))
	

func remove_piece():
	if get_piece():
		get_piece().hide()
		
func detach_piece():
	if is_instance_valid(get_piece()):
		$PieceSlot.remove_child(get_piece())
	
func set_piece_side(_side):
	get_piece().side = _side
	piece_side = _side
	
func set_state(_state):
	state = _state
	$Highlighted.hide()
	$Selected.hide()
	
	match _state:
		STATES.SELECTED : $Selected.show()
		STATES.HIGHLIGHTED: $Highlighted.show()

func _on_Square_mouse_entered():
	entered = true

func _on_Square_mouse_exited():
	entered = false

func _on_Square_input_event(_viewport, event: InputEvent, _shape_idx):
	if entered and event.is_action_pressed("select"):
		emit_signal("selected", self)
