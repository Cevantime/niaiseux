extends Node2D

export(Color) var color = Color(0.5, 0.1, 0.8, 0.5)

func _draw():
	var width = $'..'.width
	var v = Vector2(width, width)
	draw_rect(Rect2(- v / 2, v), color)
