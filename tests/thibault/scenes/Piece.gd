tool
extends Sprite

export(PieceModel.TYPES) var type = PieceModel.TYPES.TYPE_GENIUS setget set_type
export(PieceModel.SIDES) var side = PieceModel.SIDES.WHITE setget set_side

onready var piece_model = $PieceModel

const tex_mapping = {
	0 : 'dummy',
	1 : 'nully',
	2 : 'genius'
}

func _ready():
	set_side(side)
	set_type(type)

func _process(_delta):
	if !piece_model:
		piece_model = $PieceModel
		
func get_square():
	return get_parent().get_parent()

func set_type(_type):
	texture = load("res://tests/thibault/assets/%s.svg" % [tex_mapping[_type]])
	type = _type
	if is_instance_valid(piece_model):
		piece_model.type = _type
	
func set_side(_side):
	modulate = Color.white if _side == PieceModel.SIDES.WHITE else Color.darkgreen
	side = _side
	if is_instance_valid(piece_model):
		piece_model.side = _side


func _on_PieceModel_type_changed(new_type):
	texture = load("res://tests/thibault/assets/%s.svg" % [tex_mapping[new_type]])
