tool
extends Node2D

func _draw():
	var width = $'..'.width
	var color = $'..'.color
	var v = Vector2(width, width)
	draw_rect(Rect2(- v / 2, v), color)
