extends Control

var basic_game_packed_scene = preload("res://tests/thibault/LocalBasicGame.tscn")
var human_player_packed_scene = preload("res://models/player/HumanPlayer.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TwoPlayersLocalButton_pressed():
	var two_player_scene = basic_game_packed_scene.instance()
	var white_player = human_player_packed_scene.instance()
	var black_player = human_player_packed_scene.instance()
	two_player_scene.get_game().set_player_black(black_player)
	two_player_scene.get_game().set_player_white(white_player)
	
	var root = get_node("/root")
	root.get_child(root.get_child_count() - 1).queue_free()
	
	get_node("/root").add_child(two_player_scene)
	
	two_player_scene.get_game().start()


func _on_ComputerButton_pressed():
	get_tree().change_scene("res://tests/thibault/ChooseSideMenu.tscn")


func _on_NetworkButton_pressed():
	get_tree().change_scene("res://tests/thibault/MultiPlayerBasicGame.tscn")
