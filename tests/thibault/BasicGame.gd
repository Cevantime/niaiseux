extends Node

signal human_moved(from, to)

var square_to_play
var human_interaction_enabled = false

var reversed = false

func _ready():
	get_game().connect("enable_human_interaction", self, "_on_GameModel_enable_human_interaction")
	get_game().connect("disable_human_interaction", self, "_on_GameModel_disable_human_interaction")
	get_game().connect("game_started", self, "_on_game_started")
	get_game().board_model.connect("piece_moved", self, "_on_Board_move_done")
	
	for square in $CanvasLayer2/Board/Squares.get_children():
		square.connect("selected", self, "_on_square_selected")
	
func get_game() -> GameModel:
	return $GameModel as GameModel

func _on_GameModel_enable_human_interaction():
	human_interaction_enabled = true
	
func _on_GameModel_disable_human_interaction():
	human_interaction_enabled = false

func _on_square_selected(square):
	if not human_interaction_enabled:
		return
	var board_model = get_game().board_model
	if square_to_play and square.state == Square.STATES.HIGHLIGHTED:
		var from = [square_to_play.square_model.pos_x, square_to_play.square_model.pos_y]
		var to = [square.square_model.pos_x, square.square_model.pos_y]
		board_model.request_move(square_to_play.get_piece().piece_model, square_to_play.square_model, square.square_model)
		emit_signal("human_moved", from, to)
		square_to_play = null
		return
			
	var piece = square.square_model.get_piece()
	if !piece:
		return
	square_to_play = square
	var availables = get_game().referee.get_available_squares_for_piece(piece)
	for s in $CanvasLayer2/Board/Squares.get_children():
		if availables.has(s.square_model):
			s.set_state(Square.STATES.HIGHLIGHTED)
		else: s.set_state(Square.STATES.IDLE)
	
	square.set_state(Square.STATES.SELECTED)

func _on_Board_move_done(_piece_from, _square_model_from, square_model_to, board_model):
	for s in $CanvasLayer2/Board/Squares.get_children():
		if s.square_model != square_model_to : s.set_state(Square.STATES.IDLE)
		else : s.set_state(Square.STATES.SELECTED)

func set_reversed(_reversed):
	reversed = _reversed
	var reversed_int = int(_reversed)
	get_node('CanvasLayer2').rotation_degrees = 180 * reversed_int
	get_node('CanvasLayer2').offset = Vector2(600 *reversed_int, 600 * reversed_int)
	for s in get_node('CanvasLayer2/Board/Squares').get_children():
		s.get_piece().scale.y = abs(s.get_piece().scale.y) * (1 - 2 * reversed_int)
	

func _on_GameModel_game_started():
	$TimeUpdate.start()
	
func set_player1(name1):
	$CanvasLayer4/Panel/VBoxContainer/Player1Name.text = name1
	
func set_player2(name2):
	$CanvasLayer4/Panel/VBoxContainer/Player2Name.text = name2

func _on_game_started():
	var time_white = get_game().milliseconds_for_white
	var time_black = get_game().milliseconds_for_black
	var time_white_display
	var time_black_display
	
	if reversed : 
		time_black_display = $CanvasLayer4/Panel/VBoxContainer/Player1Time
		time_white_display = $CanvasLayer4/Panel/VBoxContainer/Player2Time
		
	else : 
		time_black_display = $CanvasLayer4/Panel/VBoxContainer/Player2Time
		time_white_display = $CanvasLayer4/Panel/VBoxContainer/Player1Time
	
	if time_white < 0:
		time_white_display.hide()
		
	if time_black < 0:
		time_black_display.hide()
	

func _on_TimeUpdate_timeout():
	var time_white = get_game().referee.get_remaining_time(0)
	var time_black = get_game().referee.get_remaining_time(1)
	var time_white_display
	var time_black_display
	
	if reversed : 
		time_black_display = $CanvasLayer4/Panel/VBoxContainer/Player1Time
		time_white_display = $CanvasLayer4/Panel/VBoxContainer/Player2Time
		
	else : 
		time_black_display = $CanvasLayer4/Panel/VBoxContainer/Player2Time
		time_white_display = $CanvasLayer4/Panel/VBoxContainer/Player1Time
		
	time_white_display.text = format_time(time_white)
	time_black_display.text = format_time(time_black)
	
	
func format_time(msecs):
	if msecs < 10000:
		return "%1.1f" % [int(msecs/100.0) / 10.0]
	
	var secs = int(msecs / 1000)
	
	if secs < 3600:
		return "%02d:%02d" % [int(secs / 60), secs % 60]
	if secs < 48000:
		var minutes = int(secs / 60)
		return "%02d:%02d:%02sd" % [int(minutes / 60), minutes % 60, secs % 60]
