extends Node

class_name GameModel

export(NodePath) var player_white_path
export(NodePath) var player_black_path
export(NodePath) var referee_path
export(NodePath) var board_model_path
export(int, -1, 100000000) var milliseconds_for_white = -1
export(int, -1, 100000000) var milliseconds_for_black = -1
export(int, -1, 100000000) var inc_milliseconds_for_white = 0
export(int, -1, 100000000) var inc_milliseconds_for_black = 0

signal enable_human_interaction
signal disable_human_interaction
signal game_started

var player_white setget set_player_white
var player_black setget set_player_black
var referee setget set_referee
var board_model: BoardModel

var turn = PieceModel.SIDES.WHITE setget set_turn
var over = false
var move_count = 0

var players = {}

func _ready():
	referee = get_node(referee_path)
	if !referee :
		printerr("Invalid assigned referee")
	board_model = get_node(board_model_path)
	if !board_model :
		printerr("Invalid assigned board_model")
		
	referee.game = self
	
	if player_white_path:
		set_player_white(get_node(player_white_path))
		
	if player_black_path:
		set_player_black(get_node(player_black_path))
		
func set_player_white(player):
	if !player :
		printerr("Invalid assigned player white")
	player_white = player
	player_white.game = self
	player_white.initialize();
	players[0] = player_white
	
func set_player_black(player):
	if !player :
		printerr("Invalid assigned player black")
	player_black = player
	player_black.game = self
	player_black.initialize()
	players[1] = player_black

func set_referee(_referee):
	referee = _referee
	_referee.game = self
	
func set_turn(_turn):
	move_count += 1
	current_player().disable()
	turn = _turn
	if not over:
		current_player().enable()

func start():
	referee.start()
	emit_signal("game_started")
	current_player().enable()
	
func current_player():
	return players[turn]
	

	
