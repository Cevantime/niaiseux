extends Node

export(int, 0, 20) var depth = 1

signal moves_found

var should_move = false

class Move:
	var piece_from
	var square_from
	var piece_to
	var square_to
	var score

class ScoreSorter:
	static func compare_dec(m1, m2):
		return m1.score > m2.score
	static func compare_acc(m1, m2):
		return m1.score < m2.score
		
func think(game):
	should_move = false
	var turn = game.turn
	var moves = get_moves(game, turn)
	for m in moves:
		play_move(m, game)
		m.score = alpha_beta(game, (turn + 1) % 2, -100000, 1)
		reset_move(m, game)
		if should_move:
			break
	
	moves.sort_custom(ScoreSorter,"compare_dec")
	print("\n")
	emit_signal("moves_found", moves)

func print_move(m):
	print("%s %s -> %s %s : %s" % [
		m.square_from.pos_x, 
		m.square_from.pos_y, 
		m.square_to.pos_x, 
		m.square_to.pos_y,
		m.score
	])

func print_board(board_model): 
	for j in range(board_model.height):
		var p = ''
		for i in range(board_model.width):
			var piece = board_model.get_square(i, j).get_piece()
			p += str(piece.type) if piece else '.'
		print(p)
		
	print("\n")
	
func get_moves(game, turn):
	var board = game.board_model.board
	var moves = []
	for x in range(game.board_model.width):
		for y in range(game.board_model.height):
			var square = game.board_model.get_square(x, y)
			var piece = square.get_piece()
			if ! piece:
				continue
			if piece.side == turn:
				for s in game.referee.get_available_squares_for_piece(piece, turn):
					var m = Move.new()
					m.piece_from = piece
					m.square_from = square
					m.piece_to = s.get_piece()
					m.square_to = s
					moves.push_back(m)
	return moves
	
func alpha_beta(game, turn, minimum, current_depth):
	var board = game.board_model.board
	if game.over || current_depth == depth:
		return evaluate(game) * (-1 if turn == game.turn else 1)
	else :
		var j = -100000
		for m in get_moves(game, turn):
			play_move(m, game)
			j = max(j, alpha_beta(game, (turn + 1) % 2, j, current_depth + 1))
			reset_move(m, game)
			if -j <= minimum:
				return -j
		
		return -j

func play_move(m: Move, game):
	m.square_from.piece = null
	m.square_to.piece = m.piece_from
	if m.piece_to and m.piece_to.type == PieceModel.TYPES.TYPE_GENIUS:
		game.over = true

func reset_move(m: Move, game):
	m.square_from.piece = m.piece_from
	m.square_to.piece = m.piece_to
	if m.piece_to and m.piece_to.type == PieceModel.TYPES.TYPE_GENIUS:
		game.over = false
	
func evaluate(game):
	var board = game.board_model.board
	var turn = game.turn
	var score_activity = 0
	var score_material = 0
	var genius_found = 0
	for x in range(game.board_model.width):
		for y in range(game.board_model.height):
			var square = game.board_model.get_square(x, y)
			var piece = square.get_piece()
			if !piece:
				continue
			if piece.side == turn:

				if piece.type == PieceModel.TYPES.TYPE_GENIUS:
					genius_found += 1
				elif piece.type == PieceModel.TYPES.TYPE_DUMMY:
					score_material += 1
				elif piece.type == PieceModel.TYPES.TYPE_NULLY:
					score_material += 7
				score_activity += game.referee.get_available_squares_for_piece(piece, game.turn).size()
			else:
				if piece.type == PieceModel.TYPES.TYPE_GENIUS:
					genius_found -= 1
				elif piece.type == PieceModel.TYPES.TYPE_DUMMY:
					score_material -= 1
				elif piece.type == PieceModel.TYPES.TYPE_NULLY:
					score_material -= 7
				score_activity -= game.referee.get_available_squares_for_piece(piece, (game.turn + 1) % 2).size()
	
	var score = 0.3 * score_activity + 0.7 * score_material
	score += genius_found * 10000
	return score
