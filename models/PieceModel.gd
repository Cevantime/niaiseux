tool
extends Node
class_name PieceModel

var type = TYPES.TYPE_GENIUS setget set_type
var side = SIDES.WHITE
var square = null setget set_square, get_square

signal type_changed(new_type)

enum SIDES {
	WHITE,
	BLACK
}

enum TYPES {
	TYPE_DUMMY,
	TYPE_NULLY,
	TYPE_GENIUS
}

func set_type(_type):
	type = _type
	emit_signal("type_changed", _type)
	
func set_square(_square): 
	square = weakref(_square) if (not _square is WeakRef) and _square != null else _square
	var s = get_square()
	if s != null and (s.get_piece() == null or s.get_piece() != self):
		s.piece = self
		
func get_square():
	return square.get_ref() if square != null else null
	
func get_symbol():
	return type + 3 * side
