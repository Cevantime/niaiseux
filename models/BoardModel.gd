tool
extends Node
class_name BoardModel

signal piece_moved(piece, square_from, square_to, board_model)
signal piece_taken(piece, x, y)

export var board: Array = []
export var width = 0 setget set_width
export var height = 0 setget set_height

func create_from_infos(infos):
	var inc = 0
	var str_as_bytes = infos.board_str.to_utf8()
	set_width(infos.board_width)
	set_height(infos.board_height)
	for j in range(infos.board_height):
		for i in range(infos.board_width):
			var c = str_as_bytes[inc] - 48
			inc += 1
			var s = SquareModel.new()
			set_square(i, j, s)
			
			if c == 9:
				continue
			var piece = PieceModel.new()
			var side = PieceModel.SIDES.WHITE if c < 3 else PieceModel.SIDES.BLACK
			
			if c >= 3:
				c -= 3
			
			piece.side = side
			piece.type = c
			s.set_piece(piece)

func set_width(_width):
	board.resize(_width)
	width = _width

func get_square(x, y):
	return board[x][y]

func set_square(x, y, square):
	board[x][y] = square
	square.pos_x = x
	square.pos_y = y
	square.board = self

func set_height(_height):
	for i in range(width) :
		if board[i] == null:
			board[i] = []
		board[i].resize(_height)
	height = _height
	
func get_square_pos(square):
	return [square.pos_x, square.pos_y]
	
func request_move(piece, square_from, square_to):
	if square_from.get_piece() != piece:
		printerr("Wrong piece move requested")
		return false
	var target_piece = square_to.get_piece()
	if target_piece:
		emit_signal("piece_taken", target_piece, square_to)
	square_from.set_piece(null)
	square_to.set_piece(piece)
	emit_signal("piece_moved", piece, square_from, square_to, self)
	return true
	
func is_off_board(x, y):
	return not (x >= 0 && x < width && y >= 0 && y < height)
