tool
extends Node
class_name SquareModel

var directions: int
var piece setget set_piece #, get_piece
var board setget set_board
var pos_x
var pos_y

func set_piece(_piece):
	piece = weakref(_piece) if (not _piece is WeakRef) and _piece != null else _piece
	var p = get_piece()
	if p != null and (p.get_square() == null or p.get_square() != self):
		p.square = self

func get_piece():
	return piece.get_ref() if piece != null else null

func set_board(_board):
	board = weakref(_board) if (not _board is WeakRef) and _board != null else _board
