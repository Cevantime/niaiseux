extends Node
class_name Referee

signal game_over(winner, reason, referee)

var game: GameModel setget set_game, get_game
var last_move_time
var remaining_times = [-1, -1]
var increments = [0, 0]

func set_game(_game):
	game = _game

func get_game():
	return game

func get_available_squares_for_piece(piece: PieceModel, for_turn = -1):
	var turn = for_turn if for_turn != -1 else game.turn
	
	if piece.side != turn or game.over:
		return []
		
	if piece.square == null:
		return []
	
	var square = piece.square
	
	var square_pos = game.board_model.get_square_pos(square)
	
	if ! square_pos :
		printerr("Invalid tested square")
		return []
	
	var posX = square_pos[0]
	var posY = square_pos[1]
	
	var available_squares = []
	var dirs = [128, 1, 2, 64, 4, 32, 16, 8]
	var mask = 0
	for i in range(-1, 2):
		for j in range(-1, 2):
			if i == 0 and j == 0:
				continue
			var dir = dirs[mask]
			mask += 1
			if not dir & square.directions:
				continue
			var x = posX + j
			var y = posY + i
			if piece.type == PieceModel.TYPES.TYPE_NULLY:
				while true:
					var do_break = ! add_if_available(x, y, piece, available_squares)
					if do_break:
						break
					x += j
					y += i
			else :
				add_if_available(x, y, piece, available_squares)
	
	return available_squares
	
func add_if_available(x, y, piece, available_squares):
	if game.board_model.is_off_board(x,y):
		return false 
	var neighboor_square = game.board_model.get_square(x, y)
	if neighboor_square.get_piece() and neighboor_square.get_piece().side == piece.side:
		return false
	available_squares.push_back(neighboor_square)
	return ! neighboor_square.get_piece()
	
func start():
	if game.board_model:
		game.board_model.connect("piece_moved", self, "_on_piece_moved")
		game.board_model.connect("piece_taken", self, "_on_piece_taken")
	last_move_time = OS.get_system_time_msecs()
	if game.milliseconds_for_white > 0:
		remaining_times[0] = game.milliseconds_for_white
	if game.milliseconds_for_black > 0:
		remaining_times[1] = game.milliseconds_for_black
	if game.inc_milliseconds_for_white > 0:
		increments[0] = game.inc_milliseconds_for_white
	if game.inc_milliseconds_for_black > 0:
		increments[1] = game.inc_milliseconds_for_black
	start_timer()

func start_timer():
	var time_left = get_remaining_time(game.turn)
	if time_left > 0:
		print("timer start ", time_left)
		$Timer.stop()
		$Timer.wait_time = time_left / 1000.0
		$Timer.start()
		
func _on_piece_moved(piece, _from, to, board_model):
	remaining_times[game.turn] = get_remaining_time(game.turn) + increments[game.turn]
	last_move_time = OS.get_system_time_msecs()
	game.turn = (game.turn + 1) % 2
	start_timer()
	if piece.type == PieceModel.TYPES.TYPE_DUMMY:
		var pos_to = game.board_model.get_square_pos(to)
		if piece.side == PieceModel.SIDES.WHITE and pos_to[1] == 0:
			piece.type = PieceModel.TYPES.TYPE_NULLY
		elif piece.side == PieceModel.SIDES.BLACK and pos_to[1] == game.board_model.height - 1:
			piece.type = PieceModel.TYPES.TYPE_NULLY
			
func get_remaining_time(turn):
	if turn == game.turn:
		var time_left = remaining_times[turn] - (OS.get_system_time_msecs() - last_move_time)
		return max(time_left, 0)
	
	return remaining_times[turn]

func game_over(winner, reason):
	for player in game.players.values():
		player.disable()
	game.over = true
	emit_signal("game_over", winner, reason, self)
	if not $Timer.is_stopped() : $Timer.stop()

func _on_piece_taken(piece, _square):
	if piece.type == PieceModel.TYPES.TYPE_GENIUS:
		var winner = "White" if piece.side == PieceModel.SIDES.BLACK else "Black"
		game_over(winner, "capture")
		return
	var is_null = true
	var board_model = game.board_model as BoardModel
	for i in range(0, board_model.width):
		for j in range(0, board_model.height):
			var p = board_model.get_square(i, j).get_piece()
			if p and p.type != PieceModel.TYPES.TYPE_GENIUS:
				is_null = false
				break
	if is_null:
		game_over(null, "not_enough_material")
		
func _on_Timer_timeout():
	game_over("White" if game.turn == 1 else "Black", "time")
