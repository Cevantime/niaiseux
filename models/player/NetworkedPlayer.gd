extends Player
class_name NetworkedPlayer

var client: MultiPlayerClient
var enabled

func enable():
	enabled = true
	client.connect("move_done", self, "_on_move_done")

func disable():
	enabled = false
	client.disconnect("move_done", self, "_on_move_done")

func _on_move_done(infos):
	if not enabled:
		return false
	var move_infos = infos.move
	var square_from = game.board_model.get_square(move_infos.from[0], move_infos.from[1])
	var square_to = game.board_model.get_square(move_infos.to[0], move_infos.to[1])
	var piece_from = square_from.get_piece()
	game.board_model.request_move(piece_from, square_from, square_to)
