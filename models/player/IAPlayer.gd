extends Player

var thread_thinking
var enabled

func enable():
	enabled = true
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.wait_to_finish()
	
	thread_thinking = Thread.new()
	thread_thinking.start(self, "start_thinking", game)
	$GDScriptIA.connect("moves_found", self, "_on_IA_moves_found")

func disable():
	enabled = false
	$GDScriptIA.should_move = true
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.call_deferred("wait_to_finish")

func start_thinking(game):
	$GDScriptIA.think(game)
	
func _exit_tree():
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.wait_to_finish()

func _on_IA_moves_found(moves):
	$GDScriptIA.disconnect("moves_found", self, "_on_IA_moves_found")
	
	if not enabled:
		return
	var move = moves[0]
	game.board_model.request_move(move.piece_from, move.square_from, move.square_to)
	
	print("%s %s -> %s %s : %s" % [
		move.square_from.pos_x,
		move.square_from.pos_y,
		move.square_to.pos_x,
		move.square_to.pos_y,
		move.score
	])

