extends Player
class_name HumanPlayer

func enable():
	game.emit_signal("enable_human_interaction")

func disable():
	game.emit_signal("disable_human_interaction")
