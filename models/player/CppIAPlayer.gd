extends Player

var thread_thinking
var enabled

func initialize():
	game.board_model.connect("piece_moved", $IA, "register_move")

func enable():
	enabled = true
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.wait_to_finish()
	
	thread_thinking = Thread.new()
	thread_thinking.start(self, "start_thinking", game)
	$IA.connect("moves_found", self, "_on_IA_moves_found")

func disable():
	enabled = false
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.call_deferred("wait_to_finish")

func start_thinking(game):
	$IA.think(game)
	
func _exit_tree():
	if thread_thinking and thread_thinking.is_active() : 
		thread_thinking.wait_to_finish()

func _on_IA_moves_found(moves):
	$IA.disconnect("moves_found", self, "_on_IA_moves_found")
	if not enabled:
		return false
	var move = moves[0]
	game.board_model.request_move(move.piece_from, move.square_from, move.square_to)
	print("%s %s -> %s %s : %s" % [
		move.square_from.pos_x,
		move.square_from.pos_y,
		move.square_to.pos_x,
		move.square_to.pos_y,
		move.score
	])
