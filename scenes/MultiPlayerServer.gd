extends Node

func _init():
	randomize()

class Client:
	var id
	var game_id
	var db_id
	var username
	var elo
	var game_count
	
	func serialize():
		return {
			"username": username,
			"elo": elo,
			"game_count": game_count
		}

class Challenge:
	var client_id
	var client_username
	var time
	var increment
	
class Game:
	signal game_over(winner, reason, game)
	var player_white_id
	var player_black_id
	var player_white_db_id
	var player_black_db_id
	var game: GameModel
	
	func start():
		game.referee.connect("game_over", self, "_on_game_over")
		game.start()
		
	func _on_game_over(winner, reason, referee):
		emit_signal("game_over", winner, reason, self)
	
	func serialize():
		var board_str = ''
		for j in range(game.board_model.height):
			for i in range(game.board_model.width):
				var s = game.board_model.get_square(i, j)
				if s.get_piece():
					board_str += str(s.get_piece().get_symbol())
				else :
					board_str += str(9)
				
		return {
			"player_white_id": player_white_id,
			"player_black_id": player_black_id,
			"id": get_instance_id(),
			"turn": game.turn,
			"board_width": game.board_model.width,
			"board_height": game.board_model.height,
			"board_str": board_str,
			"milliseconds_for_white": game.milliseconds_for_white,
			"milliseconds_for_black": game.milliseconds_for_black,
			"inc_milliseconds_for_white": game.inc_milliseconds_for_white,
			"inc_milliseconds_for_black": game.inc_milliseconds_for_black
		}
	
# The port we will listen to
const PORT = 9080
# Our WebSocketServer instance
var _server = WebSocketServer.new()

var clients_connected = {}
var challenges = {}
var clients_playing = {}
var games = {}
var referee_packed = preload("res://models/Referee.tscn")
var SQLiteScript = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var db

func _ready():
	db = SQLiteScript.new()
	var database_path = "user://db"
	
	var file = File.new()
	var first_run = not file.file_exists(database_path)
	db.path = database_path
	db.open_db()
	
	if first_run:
		db.create_table("user", {
			"id": {"data_type":"int", "primary_key": true, "not_null": true},
			"username" : {"data_type":"char(50)"},
			"password" : {"data_type":"char(50)"},
			"salt": {"data_type": "char(50)"},
			"elo": {"data_type":"int", "not_null": true},
			"game_count": {"data_type":"int", "not_null": true},
		})
	# Connect base signals to get notified of new client connections,
	# disconnections, and disconnect requests.
	_server.connect("client_connected", self, "_connected")
	_server.connect("client_disconnected", self, "_disconnected")
	_server.connect("client_close_request", self, "_close_request")
	# This signal is emitted when not using the Multiplayer API every time a
	# full packet is received.
	# Alternatively, you could check get_peer(PEER_ID).get_available_packets()
	# in a loop for each connected peer.
	_server.connect("data_received", self, "_on_data")
	# Start listening on the given port.
	var err = _server.listen(PORT)
	if err != OK:
		print("Unable to start server")
		set_process(false)

func _connected(id, proto):
	# This is called when a new peer connects, "id" will be the assigned peer id,
	# "proto" will be the selected WebSocket sub-protocol (which is optional)
	print("Client %d connected with protocol: %s" % [id, proto])

func start_game(player_white, player_black, challenge):
	var game = Game.new()
	player_black.game_id = game.get_instance_id()
	player_white.game_id = game.get_instance_id()
	var game_model = GameModel.new()
	game_model.referee = referee_packed.instance()
	game_model.set_player_white(Player.new()) # dummy player
	game_model.set_player_black(Player.new()) # dummy player
	var board_model = BoardModel.new()
	board_model.create_from_infos({
		"board_width": 7,
		"board_height": 8,
		"board_str":"94454493333333999999999999999999999999999900000009112119"
	})
	game_model.board_model = board_model
	game.game = game_model
	game.game.milliseconds_for_black = challenge.time
	game.game.milliseconds_for_white = challenge.time
	game.game.inc_milliseconds_for_white = challenge.increment
	game.game.inc_milliseconds_for_black = challenge.increment
	game.player_black_id = player_black.id
	game.player_white_id = player_white.id
	game.player_black_db_id = player_black.db_id
	game.player_white_db_id = player_white.db_id
	clients_playing[player_white.id] = player_white 
	clients_playing[player_black.id] = player_black
	send_msg(player_white.id, "join_game", {"game":game.serialize(), "is_white" : true, "opponent": player_black.serialize()})
	send_msg(player_black.id, "join_game", {"game":game.serialize(), "is_white" : false, "opponent": player_white.serialize()})
	broadcast_msg("challenges", {"challenges": serialize_challenges()})
	add_child(game_model.referee)
	games[game.get_instance_id()] = game
	game.connect("game_over", self, "_on_game_over")
	game.start()

func _close_request(id, code, reason):
	# This is called when a client notifies that it wishes to close the connection,
	# providing a reason string and close code.
	print("Client %d disconnecting with code: %d, reason: %s" % [id, code, reason])
	

func _disconnected(id, was_clean = false):
	# This is called when a client disconnects, "id" will be the one of the
	# disconnecting client, "was_clean" will tell you if the disconnection
	# was correctly notified by the remote peer before closing the socket.
	print("Client %d disconnected, clean: %s" % [id, str(was_clean)])
	clients_connected.erase(id)
	cancel_client_challenge(id)
	
func serialize_challenges():
	var challenges_arr = []
	for challenge in challenges.values():
		challenges_arr.push_back({
			"client_id": challenge.client_id,
			"client_username": challenge.client_username,
			"time": challenge.time,
			"increment": challenge.increment,
			"id": challenge.get_instance_id()
		})
	return challenges_arr

func _on_data(id):
	# Print the received packet, you MUST always use get_peer(id).get_packet to receive data,
	# and not get_packet directly when not using the MultiplayerAPI.
	var msg = get_msg(id)
	match msg.type:
		"move_done": 
			if not clients_playing.has(id):
				printerr("Client that moved is not actually playing")
				return
			var client = clients_playing[id]
			if not games.has(client.game_id):
				printerr("Game in which move occurred is not an active game")
				return
			var game = games[client.game_id] as Game
			var board_model = game.game.board_model
			var move = msg.data.move
			var square_from = board_model.get_square(move.from[0], move.from[1])
			var square_to = board_model.get_square(move.to[0], move.to[1])
			board_model.request_move(square_from.get_piece(), square_from, square_to)
			send_msg(game.player_white_id if id == game.player_black_id else game.player_black_id, "move_done", {
				"move": move,
				"remaining_times": game.game.referee.remaining_times
			})
		"create_challenge":
			for challenge in challenges.values():
				if challenge.client_id == id:
					send_msg("challenge_already_exists", id, {})
					return
			var challenge = Challenge.new()
			challenge.client_id = id
			challenge.client_username = clients_connected[id].username
			challenge.time = msg.data.time
			challenge.increment = msg.data.increment
			challenges[challenge.get_instance_id()] = challenge
			broadcast_msg("challenges", {"challenges": serialize_challenges()})
				
		"request_challenges":
			send_msg(id, "challenges", {"challenges": serialize_challenges()})
		"accept_challenge":
			if not challenges.has(int(msg.data.challenge_id)) :
				send_msg(id, "challenge_not_exists", {
					"challenge_id": msg.data.challenge_id
				})
				return
			var challenge = challenges[int(msg.data.challenge_id)]
			challenges.erase(int(msg.data.challenge_id))
			var rand_side = randi() % 2
			var player1 = clients_connected[id]
			var player2 = clients_connected[challenge.client_id]
			var player_white = player1 if rand_side == 0 else player2
			var player_black = player2 if rand_side == 0 else player1
			start_game(player_white, player_black, challenge)
		"cancel_challenge":
			cancel_client_challenge(id)
		"create_profile":
			if clients_connected.has(id):
				printerr("There already is an id associated with the connected id")
				return
			
			var salt = str(OS.get_system_time_msecs()).md5_text()
			var password = hash_password(msg.data.password, salt)
			var elo = 1200
			var infos = {
				"username" : msg.data.username,
				"password" : password,
				"salt": salt,
				"elo": elo,
				"game_count" : 0
			}
			
			db.insert_row("user", infos)
			
			db.query("SELECT last_insert_rowid() as new_id")
			var new_id = db.query_result[0]['new_id']
			
			infos.id = new_id
			
			connect_client(id, infos)
			send_msg(id, "login_successfull", {"infos": infos})
			
		"login":
			var client_infos = get_client_from_username(msg.data.username)
			if not client_infos:
				send_msg(id, "invalid_credentials", {})
			var password = hash_password(msg.data.password, client_infos.salt)
			if password != client_infos.password:
				send_msg(id, "invalid_credentials", {})
				return
			var client = connect_client(id, client_infos)
			if clients_playing.has(id):
				var game = games[clients_playing[id].game_id]
				client.game_id = game.get_instance_id()
				if game:
					send_msg(id, "join_game", game.serialize())
					return
			send_msg(id, "login_successfull", {"infos": client_infos})
				
			
func get_client_from_username(username):
	var arr = db.select_rows("user", "username = \"%s\"" % username, ["*"])
	if arr.empty():
		return null
	return arr[0]

func connect_client(id, client_infos):
	var client = Client.new()
	client.id = id
	client.db_id = client_infos.id;
	client.username = client_infos.username
	client.elo = client_infos.elo
	client.game_count = client_infos.game_count
	clients_connected[id] = client
	return client
	
func hash_password(password: String, salt):
	return (salt.md5_text() + password).md5_text()
	
func cancel_client_challenge(client_id) :
	for challenge in challenges.values():
		if challenge.client_id == client_id:
			challenges.erase(challenge.get_instance_id())
			broadcast_msg("challenges", {"challenges": serialize_challenges()})
			return
func _process(delta):
	# Call this in _process or _physics_process.
	# Data transfer, and signals emission will only happen when calling this function.
	_server.poll()
	
func get_msg(id):
	return JSON.parse(_server.get_peer(id).get_packet().get_string_from_utf8()).result

func broadcast_msg(type, msg):
	for client in clients_connected.values():
		send_msg(client.id, type, msg)

func send_msg(id, type, msg):
	if clients_connected.has(id): # TODO: Perf Warning !!!!
		_server.get_peer(id).put_packet(JSON.print({"type": type, "data" : msg}).to_utf8())

func _on_game_over(winner, reason, game: Game):
	games.erase(game.get_instance_id())
	remove_child(game.game.referee)
	var players_data = [
		{"id": game.player_white_id, "db_id": game.player_white_db_id},
		{"id": game.player_black_id, "db_id": game.player_black_db_id},
	]
	var i = 0
	for player_data in players_data:
		player_data.infos = db.select_rows("user", "id = %s" % player_data.db_id, ['*'])[0]
		
	for player_data in players_data:
		var K = 40
		if player_data.infos.game_count >= 39:
			if player_data.infos.elo < 2400:
				K = 20
			else : 
				K = 10
		var W = 0.5
		if winner == "White":
			W = 1 if i == 0 else 0
		elif winner == "Black":
			W = 0 if i == 0 else 1
		var elo = player_data.infos.elo + K * (W - (1 / (1 + pow(10, (players_data[(i + 1) % 2].infos.elo - player_data.infos.elo)/400))))
		
		db.update_rows("user", "id = %s" % [player_data.db_id], {"elo" : elo, "game_count": player_data.infos.game_count + 1})
		
		if clients_playing.has(player_data.id):
			clients_playing[player_data.id].elo = elo
			clients_playing.erase(player_data.id)
			
		send_msg(player_data.id, "game_over", {
			"winner": winner,
			"reason": reason,
			"move_count": game.game.move_count,
			"new_elo": elo
		})
		
		i += 1
		
	
