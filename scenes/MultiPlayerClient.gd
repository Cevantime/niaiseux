extends Node
class_name MultiPlayerClient

signal connected_to_server
signal disconnected_from_server
signal game_joined(game_infos, is_white, opponent)
signal move_done(move_infos)
signal challenges_obtained(challenges)
signal game_over(infos)
signal login_successfull()

# The URL we will connect to
export var websocket_url = "ws://localhost:9080"

# Our WebSocketClient instance
var _client = WebSocketClient.new()

func _ready():
	# Connect base signals to get notified of connection open, close, and errors.
	_client.connect("connection_closed", self, "_closed")
	_client.connect("connection_error", self, "_closed")
	_client.connect("connection_established", self, "_connected")
	_client.connect("server_disconnected", self, "_disconnected")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_client.connect("data_received", self, "_on_data")

	# Initiate connection to the given URL.
	var err = _client.connect_to_url(websocket_url)
	if err != OK:
		print("Unable to connect")
		set_process(false)

func _closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	print("Closed, clean: ", was_clean)
	set_process(false)

func _connected(proto = ""):
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	print("Connected with protocol: ", proto)
	emit_signal("connected_to_server")
	
func _disconnected():
	print("disconnected from server")
	emit_signal("disconnected_from_server")

func _on_data(): 
	var msg = get_msg()
	print("Got data from server: ", msg)
	match msg.type:
		"join_game" : emit_signal("game_joined", msg.data.game, msg.data.is_white, msg.data.opponent)
		"move_done" : emit_signal("move_done", msg.data) 
		"challenges" : emit_signal("challenges_obtained", msg.data.challenges)
		"game_over" : emit_signal("game_over", msg.data)
		"login_successfull": emit_signal("login_successfull", msg.data.infos)

func _process(delta):
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	_client.poll()

func send_msg(type, msg):
	_client.get_peer(1).put_packet(JSON.print({"type" : type, "data" : msg}).to_utf8())

func get_msg():
	return JSON.parse(_client.get_peer(1).get_packet().get_string_from_utf8()).result
